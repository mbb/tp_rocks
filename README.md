# TP Rocks Cluster 19/01/2016

Installation et exploitation d'un cluster de calcul (18-22 janvier 2016)

Sainte-Foy-Lès-Lyon, 69


Vous pouvez cloner cette documentation avec git et la recompiler avec [gitbook](https://github.com/GitbookIO/gitbook):
```bash
git clone http://gitlab.mbb.univ-montp2.fr/mbb/tp_rocks.git
```

-------

De manière générale, les données du TP se trouvent [ici](http://kimura.univ-montp2.fr/tp_rocks/)
et cette documentation (compilée) se trouve [ici](http://gitlab.mbb.univ-montp2.fr/tp_rocks/_book/index.html)


# Introduction

Dans ce TP, nous allons effectuer quelques tâches basiques d'un administrateur de cluster *RocksCluster*. Des prérequis concernant les commandes de base linux sont requis. Par ailleurs, vous devez avoir au moins 50Go de disque et 1.5Go de RAM disponibles en local car nous allons faire des tests en machines virtuelles.

Le but est de pouvoir faire rapidement le tour de la distribution RocksCluster basée sur Centos et une surcouche _maison_ Rocks.

[Site Web de RocksCluster](http://www.rocksclusters.org)

Dans ce TP, nous utiliserons le gestionnaire de batch *SGE* (ou plus exactement, un de ses forks, *OGS* (à cause au rachat de _Sun_ par _Oracle_)). Pour le monitoring, *ganglia* pourra être utilisé. Ces applications sont packagées de base dans le "Jumbo DVD" de Rocks Cluster ou peuvent être téléchargées indépendemment.


> **Note** Ces packages sont des *Rolls* du point de vue de Rocks Cluster.

## Creation du noeud maitre

Il est possible de sauter cette partie et de récupérer directement une image de la machine à l'adresse suivante (ainsi, vous ne perdrez pas de temps en installation; _bien que ça soit un peu instructif_):

[fichier OVA](ftp://ngsisem.mbb.univ-montp2.fr/mbbteam/tp_rocks/rocksmaster2.ova)
[Hash md5 correspondant](ftp://ngsisem.mbb.univ-montp2.fr/mbbteam/tp_rocks/rocksmaster2.ova.md5)

Le mot de passe root est "rocks". Les packages _rpm_ nécessaires au bon fonctionnement de ce TP sont déjà inclus dans cette image (dans **/root/Downloads**).


----
## Aide/Contact


liste de diffusion RocksCluster:  npaci-rocks-discussion _at_ sdsc _dot_ edu


mon mail: remy _dot_ dernat _at_ umontpellier _dot_ fr

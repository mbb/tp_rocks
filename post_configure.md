# Post-configuration


- En premier lieu, merci de créer un dossier /usr/slurm en 777.

- Rajouter ensuite ce dossier au fichier /etc/exports puis rechargez NFS.

<button class="section bttnrem" target="slurmexports"></button>
<!--endsec-->

<!--sec data-title="exports" data-id="slurmexports" data-show=false ces-->
```bash
mkdir /usr/slurm
chmod 777 /usr/slurm
echo "/usr/slurm 10.1.0.0/255.255.0.0(rw,async,no_root_squash)" >> /etc/exports
exportfs -ra
```
<!--endsec-->


- Nous allons rajouter le RPM de _nethogs_ dans ```/export/rocks/install/contrib/6.2/x86_64/RPMS/``` en le récupérant avec ```yumdownloader``` (dépôt epel)


<button class="section bttnrem" target="nethogsrpm"></button>
<!--endsec-->

<!--sec data-title="nethogs" data-id="nethogsrpm" data-show=false ces-->
```bash
yumdownloader --enablerepo epel nethogs
mv nethogs* /export/rocks/install/contrib/6.2/x86_64/RPMS/
cd /export/rocks/install
```
<!--endsec-->

- Modification du fichier ```/export/rocks/install/site-profiles/6.2/nodes/extend-compute.xml```

  - Copiez le fichier d'exemple :

```bash
cd /export/rocks/install
cp site-profiles/6.2/nodes/skeleton.xml site-profiles/6.2/nodes/extend-compute.xml
```
  - Editez le fichier extend-compute.xml et rajouter, entre les balises ```</pre>``` et ```<post>``` :

```xml
<package>nethogs-0.8.0-1.el6</package>
```
  - Rajoutez également les packages nécessaires à "java1.7" et "java1.8".

  - Dans la section ```<post>```, rajoutez:
```
    mkdir /scratch
    chmod 777 /scratch
```
=> Nous rajoutons un dossier _scratch_ qui permettra à nos utilisateurs de faire éventuellement du _File Staging_. Dans ce cas, il peut être intéressant de coupler le dossier _scratch_ à un [partitionnement des noeuds particulier](http://www.rocksclusters.org/roll-documentation/base/5.4/customization-partitioning.html).


- puis:
```xml
<file name="/etc/hosts" mode="append" >
8.8.8.8 dnsgoogle
</file>
<file name="/etc/fstab" mode="append" >
cluster.local:/usr/slurm /slurm nfs,nobootwait,noauto defaults 0 0
</file>
```

> **Note** Il est possible de modifier la liste des hôtes connus sur le noeud maître en modifiant ```/etc/hosts.local```. Puis, en synchronisant avec ```rocks sync config```, le fichier ```/etc/hosts``` sera mis à jour.


- Vérifions que le fichier produit est bien un fichier xml valide (la sortie doit être vide) :

```bash
xmllint --noout /export/rocks/install/site-profiles/6.2/nodes/extend-compute.xml
```

- On demande à Rocks de recompiler la distribution et de réinstaller compute-0-0 :
```bash
cd /export/rocks/install
rocks create distro
rocks list host boot
rocks set host boot compute-0-0 action=install
rocks list host boot
ssh compute-0-0 reboot
ping compute-0-0
```

Pour toute modification nécessitant la réinstallation des noeuds, il faudra certainement recompiler la distribution (sauf solution _maison_) avec :
```bash
cd /export/rocks/install
rocks create distro
```


Lorsque le ping abouti, on vérifie que tout se passe bien :
```bash
Ctrl+C
rocks-console compute-0-0 &
```

Si ça se passe mal, vous pouvez accéder à un shell par SSH sur le port 2200 sur l'hôte, dans un environnement chrooté, dans ```/mnt/sysimage```

Les logs intéressants se trouvent dans ```/tmp```, ```/var/log``` et ```/root```, une fois l'installation finie :
```
{/mnt/sysimage}/var/log/rocks-install.log
/root/install.log*
/var/log/anaconda.*.log
...
```

Pour plus d'informations de debug, voir [ici](../files/debug_rocks.pdf)



Quand l'installation est terminée :

```bash
ssh compute-0-0
cat /etc/hosts
ping -c2 dnsgoogle
ls -al /scratch
nethogs
```

### Visualisation du schéma d'installation

Voir le graphe kickstart ici: http://127.0.0.1/misc/dot-graph.php

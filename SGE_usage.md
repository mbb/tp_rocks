# Mon premier vrai job SGE

Nous avons vu dans le chapitre [Premiers pas](first_steps.md) un premier job avec un simple _sleep_.

Nous allons faire désormais un vrai script de soumission SGE.

Pour cela, il nous d'abord récupérer le RPM de gnuplot (en root) et un script gnuplot (en tant que toto) pour tracer dans un espace tridimensionnel un nuage de points.

```bash
yum search --enablerepo epel gnuplot
yum --enablerepo epel install gnuplot44-minimal.x86_64
rpm -qa |grep gnuplot
yumdownloader --enablerepo epel gnuplot44-common.x86_64 gnuplot44-minimal.x86_64

su - toto
wget -O demo_4.4_random.5.gnu http://gnuplot.sourceforge.net/demo_4.4/random.5.gnu
exit
```

Il nous faudrait rajouter maintenant ces 2 packages gnuplot au fichier extend-compute.xml (dans des balises ```<packages>```) et les fichiers RPM au dossier /export/rocks/install/contrib/6.2/x86_64/RPMS pour qu'il soit résintallé à chaque réinstallation.


Cependant, nous allons nous contenter de déployer et installer les RPM ce coup-ci, pour nous éviter une réinstallation des noeuds (cependant, c'est ce qu'il faudrait faire pour pérreniser l'installation de gnuplot).

```bash
rocks iterate host compute "scp gnuplot*.rpm %:/tmp/"
rocks run host compute "rpm -ivh /tmp/gnuplot*"
```

> **Comment** Nous aurions pu nous contenter de placer le RPM dans le home directory de toto (ou un autre dossier partagé; plutôt que de copier celui-ci dans le dossier /tmp de chaque noeud). Ce dossier étant monté sur tous les noeuds par autofs.


### Mon premier script de soumission

Ouvrir un fichier texte en tant que toto que vous nommerez _general_qsub_file.sh_ et insérer ce qui suit:

```
#!/bin/bash
# generic submission file to configure for SGE
# Beginning of SGE Options (all options begin with '#$')
# Define shell to use for this job (/bin/sh here)
#$ -S /bin/bash
# Job name
#$ -N my_sge_test
# Using current working directory (otherwise, you will have to use '#$ wd /path/to/run')
#$ -cwd
# Specificy a specific resource value; here, the hard time limit for your job
#$ -l h_rt=12:00:00
# choose to run on a specific queue
# (qconf -sql (to list queues) qconf -sq queue_name (to print informations on this queue))
#$ -q all.q
# Get a mail when job is finished or suspended
#$ -m eb
#$ -M me@mail.com
# Redirects the standard output to the named file.
#$ -o my_sge_test.out
###$ -e my_sge_test.err
# merge standard and error outputs
#$ -j y
# choose a parallel environment and run on N slots (use $PE_HOSTFILE)
#$ -pe mpi 2
# Export all my environment variables into job runtime context
#$ -V
# other interesting options : -t n (for n tasks array), -sync y (to wait until job is finished),
# -v PATH (to export only PATH variable))
# ...
## for more informations "man qsub"

#you could export/change some environment variables before
export LD_LIBRARY_PATH=/usr/lib64/:$LD_LIBRARY_PATH

/path/to/my/executable --an_option /path/to/other/file
```

Ce script de soumission va nous servir de modèle pour nos futures soumissions. Nous nous contenterons de commenter ce qui ne nous intéresse pas et modifier celle qui nous intéressent.
Les options SGE commencent toutes par *#$*. D'autres options sont disponibles pour *qsub*. Il suffit de lire le _man_. Dans le chapitre [Premiers pas](first_steps.md), nous avions par exemple utilisé _-b y_ qui permet d'exécuter directement une commande sans passer par un script de soumission.

```bash
cp general_qsub_file.sh submit_gnuplot.sh
vim submit_gnuplot.sh
```


Modifiez le script _submit_gnuplot.sh_ de manière à lancer le script gnuplot _demo_4.4_random.5.gnu_ avec un fichier de sortie standard et un pour la sortie d'erreur. Le nom du binaire de gnuplot est _gnuplot44_. Il faut décommenter les 2 premières lignes pour que la sortie se fasse dans un fichier d'image png. En effet, il n'y a pas, par défaut, de serveur X sur les noeuds de calcul (sinon, il faudrait installer un serveur _X_ ou utiliser _Xvfb_). Vous pourrez visionner votre image avec le programme _eog_ par exemple si votre configuration de X est Ok (DISPLAY, xauth...).

<button class="section bttnrem" target="nfs_autofs"></button>
<!--endsec-->

<!--sec data-title="submit_gnuplot.sh" data-id="nfs_autofs" data-show=false ces-->
```
#!/bin/bash
#$ -S /bin/bash
#$ -N gnuplot_rem
#$ -cwd
#$ -l h_rt=12:00:00
#$ -q all.q
#$ -o gnuplot.out
#$ -e gnuplot.err
#$ -V

gnuplot44 demo_4.4_random.5.gnu
```
<!--endsec-->

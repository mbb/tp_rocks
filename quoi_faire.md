# Et maintenant (...) ? Dois-je adopter RocksCluster ?

Vous savez désormais installer un cluster classique RocksCluster. L'intérêt majeur de RocksCluster est sa simplicité, la quantité importante d'utilisateurs et la réactivité de la liste de diffusion. Cependant, de nombreux systèmes permettent également d'installer/exploiter des clusters :
- [*xCat*](http://sourceforge.net/p/xcat/wiki/Main_Page/),
- [*Warewulf*](http://warewulf.lbl.gov/trac),
- [*kadeploy*](http://kadeploy3.gforge.inria.fr/),
- [sidus](http://www.cbp.ens-lyon.fr/doku.php?id=developpement:productions:sidus),
- [FAI](http://fai-project.org/),
- [OAR](https://oar.imag.fr/),
- systèmes _maison_ (serveurs d'image avec NFS + NIS, etc...),
- etc...



Le principal problème de RocksCluster est son aspect vieillissant et sa fréquence de mises à jour.

En effet, bien qu'étant assez modulaire, il est difficile de sortir de RedHat/CentOS and co. (ScientificLinux). RocksCluster est peu souvent mis à jour:
- il faut attendre longtemps après une mise à jour de CentOS, or CentOS est déjà en retard par rapport à RedHat.
Cela peut poser des problèmes évidents de sécurité (exemples récents de failles OpenSSL ou bien bash) ou si vous souhaitez accéder à des fonctionnalités implémentées dans des versions récentes de CentOS (ex: _cgroups_ noyau / _docker_, version de _python_ (2.6, 2.7), de _yum_ et de la _glibc_ par défaut...). Tout ceci peut être changé, patché, maintenu manuellement (...) mais nécessite du temps et des compétences.

Par contre, la faible fréquence des mises à jour indique également une certaine stabilité et l'orientation CentOS/RedHat amène comme avantage tout l'écosystème existant sous RedHat, notamment la certification de certains drivers, etc...

Par ailleurs, le côté diskfull n'est pas forcément un avantage. Vous avez un scratch en local, mais pour toute modification apportée à un noeud, vous devez la déployer sur tous les noeuds et modifier la procédure d'installation de vos noeuds ce qui rend cette phase de post-installation de plus en plus longue. En revanche, vous avez également toute la configuration du noeud en local ce qui diminue  de facto le trafic réseau.


## Dois-je adopter RocksCluster ?

Cette question est donc bien plus complexe qu'il n'y paraît et sort du contexte de ce TP et reste un point de vue subjectif.

Avant d'adopter RocksCluster et de choisir le matériel pour votre cluster, il faut se poser un certain nombre de questions :
- Quels sont les moyens (humains, infrastructure ([\*](#details_infra)), financiers) affectés au cluster et à sa gestion ?
- Quel dimensionnement (taille du cluster, nombre d'utilisateurs) ?
- Quelles displines, quelles applications et quels sont les types de calculs amenés à tourner sur mon cluster ?

<a id="details_infra"></a>
(\*): _ex: en diskless on consommera moins (kW) et ça pèsera moins lourd, mais le coût de l'interconnexion sera sûrement plus élevé..._

<button class="section bttnrem" target="autres_questions"></button>
<!--endsec-->

<!--sec data-title="Autres quesions à se poser pour le matériel" data-id="autres_questions" data-show=false ces-->

Cette dernière (et pourtant simple question) en amène beaucoup d'autres :
  - Ai-je un besoin impératif de stabilité ? De quelle évolutivité ai-je besoin pour ma solution ?
  <br />
  => _C'est une question que se pose tout administrateur système (pour la sécurité, les pilotes, etc...), mais ce choix dépend aussi de la displine qu'exerce vos utilisateurs: dans certains cas on préfèrera la stabilité pour éviter de perdre des semaines de calcul, dans d'autres, on aura besoin des dernières technologies, des derniers packages et relancer un job ne sera pas un problème._
  - Dois-je privilégier des applications MPI _(choix de la connectique réseau)_ ou non ?<br />
  => _Infiniband ou pas ? Compatibilité des drivers (qlogic/mellanox...) ?_
  - Est-ce que les calculs sont des calculs simples (calculs flottants/entiers) ?
  - Dois-je privilégier la mémoire aux nombres de coeurs ou l'inverse (mémoire/coeur nécessaire)?
  - Ai-je besoin de processeurs Intel ou AMD ? Quel modèle ? [Avec l'hyperthreading activé](https://software.intel.com/fr-fr/forums/software-tuning-performance-optimization-platform-monitoring/topic/480007) ?
  - Est-ce que j'ai besoin de cache pour mes applications (question des entrées/sorties) ?
  - Ai-je besoin de disques durs sur mes machines (diskfull ou diskless) ?
  - Dois-je utiliser un/des NAS NFS ou bien opter pour un système de fichier parallélisé ? Ou bien est-ce que j'utilise les deux _(ex: NFS pour les home et les applications, système de fichier parallélisé pour le scratch, etc...)_ ?
  - Dois-je utiliser un autre système de stockage ?

Pour la mise en place d'un nouveau projet, je conseille de visionner [cette vidéo de Françoise Berthoud](https://webcast.in2p3.fr/videos-162) _(même si ça date un peu)_

<!--endsec-->

> **Comment** les systèmes de fichiers parallélisés existaient il y a longtemps dans Rocks au travers de PVFS. Cependant, ce dernier n'est plus utilisé/maintenu et proposé par Rocks. Un "roll" lustre existe pour la version 6.0 de RocksCluster.

----


Cependant, RocksCluster reste une distribution Linux classique basée sur CentOS. Vous pouvez le modifier comme bon vous semble et tout le code de surcouche _maison_ Rocks est libre et modifiable à souhait. Le fichier extend-compute permet de passer en phase de post-installation n'importe quel script shell.

A l'UMR5554/ISE-M et sur la plate-forme LabEx MBB, nous utilisons un RocksCluster modifié pour nos besoins :
- pas de service 411 pour synchroniser nos utilisateurs, mais une connexion à un LDAP,
- un système de déploiement et d'hébergement de packages _maison_,
- plusieurs NAS déclarés dans Rocks (mais pas gérés par Rocks) sous debian like avec NFS/ZFS, qui hébergent les homes des utilisateurs et des applications ainsi que des librairies,
- et de nombreux autres scripts _maison_.

Nous envisageons toutefois une migration vers une solution _maison_ à base d'un serveur d'image et de distribution debian/\*, avec SLURM comme gestionnaire de batch. Cependant, cela demande de rééduquer nos utilisateurs (comment soumettre et gérer ses jobs, etc...).


Dans le domaine de la bioinformatique, les mises à jour des applications sont régulières et il est important d'avoir un système récent pour bénéficier des dernières bibliothèques.

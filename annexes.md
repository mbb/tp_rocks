# Annexes



<!-- toc -->

# Rocks

## Autres commandes rocks utiles

- Générer un profil kickstart du noeud:
```bash
rocks list host profile [compute-0-0]
```

- Afficher les attributs/propriétés d'un noeud:
```bash
rocks list host attr [compute-0-0]
```

- Rajouter manuellement quelque chose dans rocks (Frontend, NAS, interface, appliance, roll...):
```bash
rocks add ...
```
Il est possible de créer des groupes de machines dans rocks pour pouvoir déployer des images/packages/rolls différents en fonction de l'appartenance ou non à un groupe:
[Adding applicance](http://central6.rocksclusters.org/roll-documentation/base/6.1.1/customization-adding-appliance.html)



- Assigner une valeur (adresse IP, adresse Mac, hostname...) à une clé Rocks:
```bash
rocks set ...
```

- Supprimer une valeur (adresse IP, adresse Mac, hostname...) à une clé Rocks:
```bash
rocks remove ...
```
Ainsi, pour supprimer un noeud de calcul pour Rocks, il faudra faire:
```bash
rocks remove host compute-0-0
```
Et si l'on souhaite aussi le supprimer de la base des machines installées par ```insert-ethers``` (qui stocke adresses IP/adresses Mac):
```bash
insert-ethers --remove compute-0-0
```
Par ailleurs, voir [ici](#suppression_sge) pour une suppression du noeud de SGE.

- Voir aussi ```rocks report ...```, ```rocks dump ...```. Pour un référentiel des commandes rocks, voir [ici](http://www.rocksclusters.org/roll-documentation/base/5.4.3/c2083.html)


## backup rocks

- Création d'un _restore_ roll (il faut LANG en US, sinon ça plante):
```bash
cd /export/site-roll/rocks/src/roll/restore/
LANG=en\_US.iso885915
make roll
LANG=fr\_FR.UTF-8
```
-> Va créer un ISO nommé _hostname-restore-date-0.arch.disk1.iso_ qu'il faudra insérer en même temps que le choix des rolls à la réinstallation.

- Dump de la base de données:
```bash
/opt/rocks/mysql/bin/mysqldump --defaults-file=/root/.rocks.my.cnf --insert-ignore -u root cluster > /root/dump_rocksdb.sql
```

D'autres fichiers/dossiers intéressants pourront être sauvegardés:
- tous les fichiers Linux de configuration habituels situés dans /etc
- /var/named
- /var/www
- /var/lib/ganglia
- /var/411
- /export/rocks/install/site-profiles/$version/{graphs,nodes}/
- vos RPMS éventuellement dans /export/rocks/install/contrib/$version/$arch/{,S}RPMS/
...


# SGE

## service SGE


D'un point de vue *service*, SGE (_pour rappel OGS dans notre cas_) comprend le processus sge_qmaster qui tourne sur le port 536 par défaut côté noeud maître. Ce démon peut être appelé avec le script de service ```bash
cat /etc/init.d/sgemaster.`hostname -s`
```
Il est possible de relancer le service sans perdre les jobs qui tournent avec ```bash
service sgemaster.{votre hostname} softstop
``` puis ```bash
service sgemaster.{votre hostname} start
```

Du côté des esclaves, le processus sge_execd tourne sur le port 537 et correspond au script de service ```bash
/etc/init.d/sgeexecd.`cat $SGE_ROOT/$SGE_CELL/common/act_qmaster |cut -f1 -d.`
```
Vous pouvez redémarrer ce service proprement de la même manière que pour le noeud maître.

Toutes les variables d'environnement SGE sont chargées par un fichier _profile_. Dans rocks, ce fichier est ```/etc/profile.d/sge-binaries.{c,}sh```. Vous pouvez changer le port, le chemin vers SGE...

> **Comment** Dans les _Debian like_ le port par défaut est différent:
```bash
# cat /etc/services |grep sge
sge-qmaster	6444/tcp	sge_qmaster	# Grid Engine Qmaster Service
sge-qmaster	6444/udp	sge_qmaster
sge-execd	6445/tcp	sge_execd	# Grid Engine Execution Service
sge-execd	6445/udp	sge_execd
```

### Logs et debug SGE

#### Logs

Les logs de SGE se trouvent du côté du noeud maître dans le fichier ```$SGE_ROOT/$SGE_CELL/spool/qmaster/messages``` et dans ```$SGE_ROOT/$SGE_CELL/spool/compute-0-n/messages``` (ou n correspond au _rank_ du compute d'un point de vue _RocksCluster_).

Dans le cas où vous utiliseriez SGE sans RocksCluster ou que vous Modifiez la configuration par défaut, les logs peuvent aussi se trouver dans /var/spool ou /tmp.


#### Debugs

```bash
qstat -f
```
Peut renvoyer pas mal d'informations.

Vous pouvez vérifier ensuite que execd accepte les jobs sur le port 537 :

```bash
qping compute-0-0.local 537 execd 1
```

Inversement, vous pouvez aussi vérifier qu'un noeud peut se connecter au noeud maître :

```bash
ssh compute-0-0 "qping -info MASTER_NAME 536 qmaster 1"
ssh compute-0-0 "qping -f MASTER_NAME 536 qmaster 1"
```

## Autres commandes SGE utiles


> **Info** La plupart de ces autres commandes ne sont accessibles  qu'à un manager/opérateur SGE. Néanmoins, un utilisateur peut faire certaines de ces commandes sur ses propres jobs.

### qdel

Tuer un job:
```bash
qdel [-f] job_id
```
ou:
```bash
qdel -u user
```
Pour tuer tous les jobs associés à un utilisateur.

### qresub

Pour resoumettre un job:
```bash
qresub
```
> **Note** Attention à exécuter un qresub avec l'utilisateur qui a lancé le job à l'origine si vous souhaitez qu'il y ai accès...

### qalter

Pour modifier un job en attente (priorité, queue, etc...):
```bash
qalter
```

### qmod

```qmod``` Permet Modifier une queue ou un job qui tourne.

Nettoyer un job qui a le statut en erreur:
```bash
qmod -cj job_id
```

Il est également possible d'appliquer cette action sur une queue:
```bash
qmod -cq nom_de_la_queue
```

### qselect

Vous pouvez sélectionner une queue avec ```qselect```.
Dans quel but ? Quelques exemples:

Lister les noeuds d'une queue:
```bash
qselect -q nom_de_la_queue
```
Sélectionner les queues dont un utilisateur à accès:
```bash
qselect -U toto
```
Désactiver une queue:
```bash
qmod -d `qselect -q nom_de_la_queue`
```
Réactiver une queue:
```bash
qmod -e `qselect -q nom_de_la_queue`
```

### qhold

Vous pouvez suspendre un job avec ```qhold```
```bash
qhold job_id
```
Ou tous ceux d'un utilisateur avec:
```bash
qhold -u user
```

### qrls

Opposé de ```qhold```. S'utilise de la même manière.

### qrsub

Permet de faire une réservation "avancée" (Advance Reservation (AR)) de machine(s) par SGE [qrsub](http://gridscheduler.sourceforge.net/htmlman/htmlman1/qrsub.html). Il faut penser à préciser une durée maximum des jobs sur ces machines si l'on veut être en mesure d'utiliser les AR. Ainsi, il sera même possible de faire du _Backfilling_: https://arc.liv.ac.uk/repos/darcs/sge/doc/devel/rfe/resource_reservation.txt

### qrstat

Permet de voir l'état des réservations.

### qrdel

Permet de supprimer une réservation

### qacct

Information d'accounting (offre un historique/décompte des jobs selon les utilisateurs...)

## Backup SGE

Editez le fichier ```$SGE_ROOT/util/install_modules/backup_template.conf```, puis:
```bash
cd $SGE_ROOT
./inst_sge -bup -auto /opt/gridengine/util/install_modules/backup_template.conf
```

La restauration se fait simplement avec:
```bash
cd $SGE_ROOT
./inst_sge -rst
```

## A propos des options de soumission SGE

Il est possible de prédéfinir certaines options de soumission pour un utilisateur dans le fichier ```$SGE_ROOT/$SGE_CELL/sge_request```. Ca permet par exemple de définir une limite de temps, une queue par défaut, ou toute autre option.

### Autres options de soumission utiles

Tableau de jobs (utile sur des simulations pour des applications non parallélisées (ou mal)):
```#$ -t n[-m[:s]]```
avec:
- *n*: valeur du premier index si *m* est spécifié, sinon, taille du tableau
- *m*: valeur du dernier indice du tableau
- *s*: pas pour passer d'un indice à l'autre.
On récupère ensuite l'ID dans le noeud d'exécution avec la variable ```$SGE_TASK_ID```


Soumettre sur un noeud spécifique:
```#$ -l hostname=compute-0-0```

Pour attendre qu'un job se termine avant de passer à la suite (utile pour des enchainements de qsub avec des tableaux de jobs par exemple...):
```#$ -sync y```

Pour le reste, ```man qsub```.


## Noeud SGE: Suppression


 <a id="suppression_sge"></a> On désactive le noeud:
```bash
qmod -d all.q@compute-0-0.local
```
On attend que les jobs se terminent sur ce noeud, puis on arrête le démon d'exécution local:
```bash
qconf -ke compute-0-0.local
```
On le supprime de la queue où il est présent, en éditant celle-ci:
```bash
qconf -mq all.q
```
On le supprime des éventuels groupes de machines dont il est membre:
```bash
qconf -mhgrp @allhosts
```
On le supprime en tant que noeud d'exécution:
```bash
qconf -de compute-0-0.local
```
Et on le supprime éventuellement en tant que noeud de soumission:
```bash
qconf -ds compute-0-0.local
```
Enfin on supprime sa configuration:
```bash
qconf -dconf compute-0-0.local
```

# Ganglia

Il s'agit d'une application de monitoring très connue dont les données sont stockées par type, et dans le temps, dans une base RRD [fiche plume RRDtool](https://www.projet-plume.org/fiche/rrdtool).

Ce service est composé essentiellement de deux démons: ```gmond``` qui sert à recueillir les données et ```gmetad``` afin de les recueillir/agréger.

Vous pouvez accéder à l'interface de ganglia depuis le noeud maître en ouvrant un navigateur à la page http://127.0.0.1/ganglia ou depuis une autre machine en mettant son adresse IP.

Par ailleurs, il est possible de rajouter des données à monitorer en combinant Ganglia à [collectl](http://collectl.sourceforge.net/). Il faut alors rajouter un module en python _collectl.py_ à Ganglia.

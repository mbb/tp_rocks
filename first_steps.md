# Premiers pas


<!-- toc -->



## Nos premiers pas dans RocksCluster

Ouvrez une session en root avec le mot de passe choisi à l'installation, puis un terminal:
- on met le clavier en français (sauf si vous avez un qwerty !):
```bash
setxkbmap fr
```
- on vérifie quelques services:
```bash
service dhcpd status
service httpd status
service nfs status
service autofs status
```
- on vérifie le réseau externe:
```bash
ping -c2 www.google.fr
```

### Premières commandes Rocks

```bash
rocks
```
Regardez rapidement la liste des commandes disponibles.

> **Note** Il n'y a pas de _man_, ni de _--help_. Il faut donc soit s'aider de la [documentation en ligne](http://www.rocksclusters.org/roll-documentation/base/5.4.3/c2083.html), soit regarder la liste des commandes puis sous-commandes. On peut toutefois utiliser le mot clé 'help' à la fin de certaines commandes pour avoir plus d'informations, comme par exemple: ```rocks set host roll help```


```bash
which rocks
file `which rocks`
less `which rocks`
rocks list
```

On constate que la commande rocks est en python. Regardez à nouveau rapidement la liste des sous-commandes disponibles de *rocks list*.

```bash
rocks list roll
rocks list host
rocks list host interface
```

> **Warning** Si vous avez _search domain hpc.org_ dans
```/etc/resolv.conf```
il faudra commenter cette ligne car les machines créées ne pourront pas être résolues par le DNS (domaine qui existe déjà...!).


On va insérer un nouveau noeud:
```bash
insert-ethers
```
-> Regardez la liste des types d'installations disponibles puis choisissez '*Compute*'


### Premier noeud de calcul

Créez une autre VM:
 - 512Mo de RAM
 - Disque d'au moins 25Go
 - Configurez le réseau pour que la "Carte 1" soit sous réseau interne (nommé intnet (_ROCKSnet_ sur les VMs) ou "_local-x_" si vous avez utilisé le script python de création de VM) avec mode proscuité autorisé pour les VMs.
 - Configurez le boot (menu système) pour que l'ordre d'amorçage prenne en charge le réseau et soit en premier dans la liste.
 - Démarrez cette nouvelle VM.


Sur le noeud maître:
 - attendez que la VM soit détectée (qu'elle apparaisse) et que le système d'installation se lance sur le noeud (une étoile apparaît alors entre parenthèses du côté du noeud maître, dans l'interface d'*insert-ethers*).


S'il vous reste suffisament de disque, répétez l'opération de création de VM (512Mo RAM/25Go Disque) en spécifiant cette fois la position du noeud:
```bash
insert-ethers --rack=0 --rank=4
```

>**Warning** Si vous n'avez pas suffisament de disque ou de mémoire (<= 4Go au total), il est plus judicieux de rajouter la VM à posteriori dans Rocks. Pour celà, il faut la télécharger et utiliser des scripts de rajout côté noeud maître comme [celui-ci](http://kimura.univ-montp2.fr/tp_rocks/slaves_vm/add-compute.sh).
Le script rajoute dans rocks la machine, son interface et synchronise le tout. En cas de soucis de communication (soucis de clé, d'IP...) redémarrez la VM de calcul afin de réinstallez celle-ci(\*).

Pour l'installation d'un cluster rocks il convient également de désactiver le SpanningTree sur les switchs internes au cluster.

(\*) il est possible de basculer le flag d'installation des noeuds de calcul rocks ainsi:

```bash
rocks set host boot $node action=$action
# il faut ensuite redémarrer le noeud (reboot ou ether-wake si éteint...)
# on suit ensuite l'install du noeud avec (ne fonctionne pas si noeud < 768Mo de RAM car install en mode textuel):
rocsk-console $node &
```

Avec _$node_ votre noeud de calcul (ex compute-0-0), et _$action_ l'action à exécuter (os, install, memtest...). En cas de soucis d'installation, on pourra prendre la main sur le noeud par SSH sur le port 2200.

### Quelques tests

Du côté du noeud maître:
```bash
rocks list host
rocks list host interface compute-0-0
rocks list host route compute-0-0
```

Une base de données MySQL incluse dans rocks gère l'ensemble de ces informations. La synchronisation se charge d'écrire les données en base de données en fichiers de configuration (/etc/hosts, DNS...).

Il est d'ailleurs possible de l'éditer directement (attention toutefois aux multiples jointures...!).

```bash
/opt/rocks/mysql/bin/mysql --defaults-file=/root/.rocks.my.cnf -uroot cluster
mysql>show tables;
mysql>select * from nodes;
mysql>select * from rolls;
mysql>exit;
```


- on vérifie le réseau externe:
```bash
ssh compute-0-0 ping -c2 www.google.fr
```

#### Quelques réglages pratiques

Nous allons installer quelques packages supplémentaires:
```bash
yum install --enablerepo epel bash-completion yum-utils
```
Nous allons ensuite rajouter ce qui suit à notre _.bashrc_:
```
setxkbmap fr
alias tentag='tentakel -g compute '
export CO="compute"
export ROCKS_HOME="/export/rocks/install"
if [ -n "$DISPLAY" ];then
        PS1="[\u@\h \W](DISPLAY)# "
fi
```

Nous chargeons notre nouveau environnement:
```bash
exec $SHELL
ping $CO-0-0
```


### Exemple de soumission de commandes puis soumission de jobs par le gestionnaire de batch

- **tentakel** et **rocks run host**

```bash
time ssh compute-0-0 "echo toto"
time tentakel -g compute "echo toto"
time rocks run host compute "echo toto"
```

- **pssh**

```bash
yum --enablerepo epel install pssh
```

Créez un fichier hosts.txt avec compute-0-0 (et compute-0-4 sur une nouvelle ligne si vous avez rajouté le deuxième noeud).
```bash
time pssh -h hosts.txt "echo toto"
```

- **pdsh**

```bash
time pdsh -w compute-0-[0,4] "echo toto"
which pdsh
```


### Soumission par OGS SGE


*OGS* est un gestionnaire de batch; il permet d'ordonnancer les jobs ([jobs scheduler](https://en.wikipedia.org/wiki/Job_scheduler)) et de gérer l'allocation des ressources en fonction de ce qui est disponible sur l'ensemble du cluster (ressource manager). De nombreux gestionnaires de batch n'ont pas ces 2 fonctions, et ne sont pas aussi simples à utiliser/gérer que dans OGS/SGE, ce qui explique (historiquement) sa large adoption.

> **Comment** Actuellement, [SLURM](https://computing.llnl.gov/linux/slurm/) est en train de prendre le pas sur la plupart des gestionnaires de jobs. SLURM est simple, libre, dispose de nombreuses fonctionnalités (dont provisionnement docker), d'une grande communauté d'utilisateurs, et d'une meilleure prise en charge DRMAA. De surcroît, SLURM permet souvent aux utilisateurs de pouvoir réutiliser leurs propres scripts de soumission dans certains grands centres de calcul qui utilisent cet outil (vous pouvez également trouver [LoadLeveler](http://www.ibm.com/software/products/fr/tivoliworkloadschedulerloadleveler) dans certains centres de calcul, mais ce n'est pas libre (IBM. Voir [OpenLava](http://www.openlava.org/) pour un équivalent libre))...


Les jobs sont dans l'ordre:
- soumis
- placés en file d'attente (*qw* / p)
- transmis au noeud d'exécution (*t*)
- tournent si pas d'erreur (*r*)
- se terminent

Si des erreurs se produisent, le job peut passer en
- *Eqw* (Error Queue Wait): la ressource nécessaire n'est pas disponible, pas de dossier correspondant existant, etc...
- *dt* (disable transfer): le transfert vers le noeud d'exécution à échoué.
...


Premières commandes OGS/SGE:
```bash
qmon
qhost
which qhost
qstat
qsub -b y "sleep 15"
watch 'qstat '
```

http://gridscheduler.sourceforge.net/howto/basic_usage.html

- *qsub* permet de soumettre un job
- *qstat* permet d'afficher l'état des jobs et des queues (option _-j_ pour voir un job particulier, option _-f_ pour une sortie plus précise, option _-u_ pour voir juste un utilisateur, option _-s_ pour préciser juste un état, _-F_ pour voir la charge des machines, _-g c_ pour voir la charge générale des queues, etc...)
- *qhost* permet d'afficher l'état des noeuds/queues/jobs (option _-q_ pour un affichage avec les queues, _-j_ pour voir les jobs, etc...)
- *qdel* pour supprimer un/des job(s)

**Tous vos utilisateurs devront savoir - à minima - utiliser toutes ces commandes (utilisez ```man``` pour plus d'informations).**



## Premier utilisateur

Vérifiez le statut de nfs et d'autofs et les fichiers associés (on n'affiche pas les commentaires et les lignes vides).

<button class="section bttnrem" target="nfs_autofs"></button>
<!--endsec-->

<!--sec data-title="NFS et autofs" data-id="nfs_autofs" data-show=false ces-->
```bash
service nfs status
service autofs status
egrep -v "^$|^#" /etc/exports
egrep -v "^$|^#" /etc/sysconfig/autofs
egrep -v "^$|^#" /etc/auto.master
egrep -v "^$|^#" /etc/auto.home
```
<!--endsec-->

Créons désormais notre premier utilisateur:

```bash
useradd toto
passwd toto
rocks sync users
```

Cette dernière commande est très importante. Elle permet de synchroniser les utilisateurs sur tous les noeuds, et les fichiers qui sont liés à ce dernier (même les fichiers /etc/auto.\*).

Un aspect intéressant concerne également les points de montage:

```bash
mount
su - toto
mount
qrsh
```

L'exemple ci-dessus introduit une nouvelle commande, qrsh, qui demande à SGE de lancer un job de type 'ssh'. On se retrouve alors connecté par ssh sur un noeud de calcul avec l'utilisateur 'toto'.

> **Hint** Par défaut, Rocks Cluster utilise /state/partition1 qu'il monte dans /export. Ce dossier va ensuite lui servir pour ses exports nfs. Les homes directories des utilisateurs sont ensuite montés dynamiquement par autofs. Il en est de même pour le dossier /export/apps qui correspond au partage NFS /share/apps. Ce dossier permet, par convention, de stocker des applications / binaires. Cependant, rien ne vous empêche de faire vos propres nas et d'indiquer à rocks où chercher vos homes ou d'autres dossiers partagés dans /etc/auto.\*.


- La commande ```rocks sync users``` (en tant que **root** sur le noeud maître) va en fait déployer ce qui est déclaré dans /var/411/Files.mk.

Une commande équivalente pourrait être:

```bash
rocks run host compute "411get --all"
```

Voir une liste de commandes "rocks sync" (comme précédemment regardez les commandes disponibles):
```bash
rocks sync
rocks sync config
rocks sync config network
```

# Summary

* [Création du noeud maître](master_create.md)
* [Premiers pas dans Rocks](first_steps.md)
* [Utilisation de module](module.md)
* [Le service 411](411.md)
* [Post-configuration Rocks](post_configure.md)
* [Paramétrage SGE](SGE_config.md)
* [Petit job SGE](SGE_usage.md)
* [Pour aller plus loin: petite application MPI](mpi.md)
* [Et maintenant (...) ? Dois-je adopter RocksCluster ? _ Réflexions sur RocksCluster_](quoi_faire.md)
* [Annexes](annexes.md)
* [Glossaire](GLOSSARY.md)

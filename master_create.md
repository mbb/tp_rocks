# Creation du noeud maitre

- Installez *Virtualbox*
- Récupérez l'image de rocks cluster Jumbo DVD
- vérifiez éventuellement l'image avec un md5sum

Là, vous pouvez continuer à la main ou si vous avez cloné la documentation, utiliser le
script bash ```nat-add.sh```et le code python ```vbox_cluster``` également disponibles [ici](https://github.com/remyd1/cloud)

A la main:

- Créez une VM 1024Mo RAM et 25Go (30Go recommandés) de disque (VDI dynamique)
- Configurez pour avoir du réseau interne sur la "Carte 1" et un NAT sur la "Carte 2"
- Dans un terminal/console faire:
```
VBoxManage natnetwork add --netname mynet --network "10.0.3.0/24" --enable --dhcp off
VBoxManage natnetwork start --netname mynet
vboxmanage list natnets
```


> **Warning** Attention, le clavier est en qwerty

- Démarrez la VM et choisir l'image téléchargée
 - Entrez "build" au premier écran
 - Patientez jusqu'à l'écran de sélection des _Rolls_
 - Choisissez "CD/DVD-based rolls"
 - Sélectionnez les rolls "base","hpc","os","kernel","sge","ganglia"
 - Validez puis faites "Next"
 - FQDN: mettre cluster.hpc.org.tploc et laissez le reste intact
 - Configurez les interfaces:<br />
eth1 mettre en IP 10.0.3.15 Netmask 255.255.255.0
eth0 réseau privé; laissez 10.1.1.1 et 255.255.0.0 pour le masque
Passerelle 10.0.3.2 DNS 10.0.3.3
 - Changez le "time zone" pour _Europe/Paris_
 - Laissez le partitionnement auto
 - Laissez finir l'installation


----

 Au redémarrage, vous avez un noeud maître RocksCluster opérationnel. Ce dernier est composé d'un serveur HTTP, TFTP, MySQL, DNS, DHCP, Syslog, Kickstart et NFS pour l'essentiel, sans compter les Rolls additionnels (ici hpc, SGE et ganglia).

 > **Comment** A l'installation des noeuds de calcul par boot PXE, les protocoles DHCP, TFTP et HTTP seront utilisés.

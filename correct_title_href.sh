#!/bin/bash

if [ -d $1 ];then 
    echo "replacing href link with kimura website"
    title=`head -1 README.md | cut -d"#" -f2 |sed "s|^ ||g"`
    find $1 -type f -regex ".*\.\(css\|html\)" -print0 | xargs -0 sed -i "s|<a href=\"./\" >$title|<a href=\"http://cbi.isem.univ-montp2.fr\" target=\"_blank\" >$title|g"
fi

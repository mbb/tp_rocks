# Utilisation de module

[Modules](http://modules.sourceforge.net/) ou plus exactement _Environment Modules_ permet de gérer dynamiquement les variables d'environnement de vos utilisateurs. Ainsi, un utilisateur peut exécuter plusieurs versions d'un même logiciel en changeant simplement de contexte par *module*.

Nous allons récupérer _via_ ```yumdownloader``` des packages RPM pour installer java sur tous les noeuds, y compris le noeud maître. Placez les packages RPM java et leurs dépendances dans ```/export/rocks/install/contrib/6.2/x86_64/RPMS/```.

Créez des liens symboliques afin d'éviter d'utiliser, par la suite, des noms de fichier à rallonge.

<button class="section bttnrem" target="symlink"></button>
<!--endsec-->

<!--sec data-title="Openjdk" data-id="symlink" data-show=false ces-->

```bash
yum search openjdk
yum install java-1.7.0-openjdk.x86_64 java-1.8.0-openjdk.x86_64
yumdownloader tzdata-java java-1.8.0-openjdk java-1.7.0-openjdk java-1.8.0-openjdk-headless
mv *.rpm /export/rocks/install/contrib/6.2/x86_64/RPMS/
cd /export/rocks/install/contrib/6.2/x86_64/RPMS
ln -s java-1.8.0-openjdk-1.8.0*.rpm java-1.8.0-openjdk.rpm
ln -s java-1.7.0-openjdk-1.7.0*.rpm java-1.7.0-openjdk.rpm
ln -s java-1.8.0-openjdk-headless*.rpm java-1.8.0-headless.rpm
ln -s tzdata-java*.rpm tzdata-java.rpm
```
<!--endsec-->

Créez un fichier module nommé java1.8 dans /etc/modulefiles, contenant (_à adapter selon le package RPM java que vous avez installé_):

```
#%Module######################################################################
#
# java1.8 modulefile
#

proc ModulesHelp { } {
puts stderr "This modulefile defines the library paths and"
puts stderr "include paths needed to use java1.8."
puts stderr "You could use BEAST2.2.1 with that version."
puts stderr "The program java1.8"
puts stderr "is now added to your PATH."
}


set is_module_rm [module-info mode remove]

set JAVA_LEVEL 1.8
set JAVA_CURPATH /usr/lib/jvm/jre-1.8.0-openjdk-1.8.0.45.b13.el6_6.x86_64/jre
#set JAVA_CURPATH /etc/alternatives/jre_1.8.0_openjdk
setenv JAVA_HOME $JAVA_CURPATH

prepend-path PATH       $JAVA_CURPATH/bin
prepend-path LD_LIBRARY_PATH  $JAVA_CURPATH/lib

append-path  PE_PRODUCT_LIST  JAVA
```
Créez un fichier similaire pour Java1.7

Tentez de charger java1.8 puis de changer vers java1.7

<button class="section bttnrem" target="module_usage"></button>
<!--endsec-->

<!--sec data-title="module java" data-id="module_usage" data-show=false ces-->

```bash
which java
java -version
echo $PATH

module list
module avail
module show java1.8
module load java1.8

which java
java -version
echo $PATH

module list
module load java1.7

which java
java -version
echo $PATH

module rm java1.8
module unload java1.7
module list

which java
java -version
echo $PATH
```
<!--endsec-->

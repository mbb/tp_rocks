
# Paramétrage de SGE

Tout est faisable dans *qmon*. Toutefois, le paramétrage général de SGE peut se faire avec *qconf*

Nous allons créer une queue (_add queue_) spécifique:
```bash
qconf -aq test.q
```

Quittez et sauvegardez (éditeur par défaut _vi_).

Une queue permet de découper le cluster selon vos volontés. Ce découpage peut être décidé, par exemple, pour l'une des raisons suivantes:
- Matériels différents (architecture (64/32 bits), gpu, quantité de mémoires, type et nombre de coeurs...),
- Systèmes, logiciels, librairies, environnements différents,
- Règles d'usage différentes (temps de calcul, priorisations (Fair Sharing, FIFO)...),
- Utilisateurs (périmètres différents, acheteurs/décideurs différents...),
- ou pour toute autre raison que vous aurez choisi...

 Par défaut l'accès aux queues est autorisé pour tout le monde, mais vous pouvez créer des _userset_ ou des configurations pour des utilisateurs particuliers. Vous pouvez aussi autoriser certains environnements parallèles sur certaines queues et les interdire dans d'autres (ex: mpi, threads...).

Il faudrait rajouter des machines à cette queue pour qu'elle soit fonctionnelle.

 > **Info** Par défaut, seule la queue _all.q_ existe. Elle contient l'ensemble des noeuds d'exécution. Dans votre cas, si vous n'avez qu'un seul noeud d'exécution, l'intérêt d'avoir plusieurs queues est nul.

- Vous pouvez lister les queues (_show queue list_) avec:
```bash
qconf -sql
```

> **Warning** Attention, si la configuration des noeuds évolue : les nouveaux noeuds sont rajoutés dans all.q, mais les anciens ne sont pas supprimés ! Cela implique que SGE cherche à contacter ces noeuds absents avant de lancer des nouveaux jobs. Il faudra donc penser à les supprimer proprement de SGE.


- Ou la définition d'une queue particulière (_show Queue_) avec:
```bash
qconf -sq test.q
```

- Il est possible d'éditer la queue (_modify Queue_) avec:
```bash
qconf -mq test.q
```
Toutefois, nous arrivons sur un éditeur avec des options qui nous sont encore inconnues. Nous allons donc continuer avec *qmon*. Sachez toutefois que la documentation en ligne est très bien faite. Vous pouvez aussi utiliser le _man_.

- Nous allons supprimer la queue précédente (_delete queue_):
```bash
qconf -dq test.q
```

> **Comment** On pourra faire de même pour lister, détruire, ajouter, modifier des utilisateurs, des groupes, des managers, des règles, des noeuds (d'administration, de soumission, d'exécution), des environnements parallèles, etc...


```bash
qmon &
```

Explorer l'interface. Les premiers points d'entrée intéressants sont:
- "Host" pour voir nos noeuds,
- "Job Control" afin de voir les jobs en attente/qui tournent/sont finis,
- "User" pour gérer des groupes (_userset_) ou utilisateurs SGE particuliers ou bien des opérateurs/managers, etc...
- "Queue Control" pour gérer vos queues.

Vous pouvez être amené à utiliser tous les autres menus SGE mais ça concerne soit de la configuration avancée (la définition de règles ou d'environnements parallèles, etc...), soit des usages particuliers (comme le checkpointing, la réservation de machines, etc...). Rapidement vous aurez surement besoin de définir des règles ou des environnements parallèles, mais ce n'est pas l'objet de ce TP.

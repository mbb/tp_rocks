#!/bin/bash
mkdir /usr/slurm
chmod 777 /usr/slurm
echo "/usr/slurm 10.1.0.0/255.255.0.0(rw,async,no_root_squash)" >> /etc/exports
exportfs -ra
rocks run host "echo 'cluster.local:/usr/slurm /slurm nfs,nobootwait,noauto defaults 0 0' >> /etc/fstab"
rocks run host "mkdir /slurm"
rocks run host "mount -a"

#!/bin/bash

#sed -i -r 's/(^<h[1-5].*)%C3%A9/\1é/g' $1
#sed -i -r 's/(^<h[1-5].*)%C3%A0/\1à/g' $1
#sed -i -r 's/(^<h[1-5].*)%C3%B4/\1ô/g' $1
#sed -i -r 's/(^<h[1-5].*)%C3%A8/\1è/g' $1
#sed -i -r 's/(^<h[1-5].*)%C3%AA/\1ê/g' $1
#sed -i -r 's/(^<h[1-5].*)%C3%B9/\1ù/g' $1
##sed -i -r 's/(^<h[1-5].*)%C3%B9/\1'

echo
echo "Processing $1 to correct toc accents problems"
echo

function correct_one_file(){
    echo "processing $1"

    awk '{
            gsub("%C3%A9","é");
            gsub("%C3%A0","à");
            gsub("%C3%A2","â");
            gsub("%C3%B4","ô");
            gsub("%C3%A8","è");
            gsub("%C3%AA","ê");
            gsub("%C3%B9","ù");
            gsub("%C3%AE","î");
            print
         }' $1 > ${1}.toc
    mv ${1}.toc $1
}

export -f correct_one_file

find $1 -name "*.html" -exec bash -c 'correct_one_file "$0"' {} \;
